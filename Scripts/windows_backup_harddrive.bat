@echo off
REM Make all files read-only
attrib +R * /S
REM Backup to external harddrive
set /p drive="Enter Drive Letter (E?): "
rclone copy ../ %drive%:/Photos --filter-from sync_filter.txt
pause
