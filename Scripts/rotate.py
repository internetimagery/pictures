# Rename all files in a folder to the same name as the folder
# Numbering each file with the format "name_000"
# Preserving any "tags" in the format [name1 name2]
# Ignoring anything in square brackets

from __future__ import print_function
try:
    import tkinter
    from tkinter import filedialog
    from tkinter import messagebox
except ImportError:
    import Tkinter as tkinter
    from Tkinter import tkFileDialog as filedialog
    from Tkinter import tkMessageBox as messagebox
import traceback
import os.path
import shutil
import os

ROOT = os.path.realpath(os.path.dirname(__file__))
# ROOT = os.getcwd()

class Main(object):

    def __init__(s):
        """ Main window! """

        window = tkinter.Tk()
        window.title("Rotate Video.")

        # Add a descriptive label
        desc_label = tkinter.Label(window, text="Roate a video.")

        # Add buttons
        cw_button = tkinter.Button(window, text="Clockwise", command=lambda: s.browse_path(90))
        ccw_button = tkinter.Button(window, text="Counter Clockwise", command=lambda: s.browse_path(-90))

        # Put it all together!
        desc_label.pack()
        cw_button.pack(side=tkinter.LEFT)
        ccw_button.pack(side=tkinter.LEFT)

        # Lets go!
        window.mainloop()

    def browse_path(s, direction):
        """ Browse to find a path """
        for video in filedialog.askopenfilenames(initialdir=ROOT):
            print(direction, video)

# ffmpeg -i src.mp4 -vf "rotate='90*PI/180:ow=ih:oh=iw'" dest.mp4
# ffmpeg -i INPUT -crf 18 -c:v libx264 OUTPUT

if __name__ == '__main__':
    try:
        Main()
    except:
        print(traceback.format_exc())
    finally:
        input("press anything to close")
