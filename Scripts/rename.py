# Rename all files in a folder to the same name as the folder
# Numbering each file with the format "name_000"
# Preserving any "tags" in the format [name1 name2]
# Ignoring anything in square brackets

from __future__ import print_function
try:
    import tkinter
    from tkinter import filedialog
    from tkinter import messagebox
except ImportError:
    import Tkinter as tkinter
    from Tkinter import tkFileDialog as filedialog
    from Tkinter import tkMessageBox as messagebox
import os.path
import shutil
import os
import re

TAGS = re.compile(r"\[.+?\]") # Extract information from tags square brackets (Tagspaces)


def rename_files(root):
    """ Rename all files in path to match the parent folder """

    # Get name of parent directory
    dir_name = os.path.basename(root) # Add a trailing underscore

    # Compile regex to search for correct naming
    check = re.compile(r"%s\_(\d+)" % re.escape(dir_name))

    # Get the files to check
    files = sorted(os.listdir(root))

    # Track the highest numbered file
    count = 0

    # Track what needs renaming and changing
    needs_rename = []
    needs_change = []


    # Check filenames for rename candidates
    for f in files:
        match = check.match(f)
        if match:
            try:
                # We have a file already renamed. Check its number as used.
                count = max(count, int(match.group(1)))
                continue
            except ValueError:
                pass

        # Failed checks. Needs a rename
        needs_rename.append(f)

    # Figure out the number of zeros to use for Numbering
    num_zeroes = len(str(count + len(needs_rename))) # Number of zeros to put ahead of numbers
    if num_zeroes < 3:
        num_zeroes = 3

    # Generate some new file paths
    for i, f in enumerate(needs_rename):

        path = os.path.join(root, f)
        if os.path.isfile(path):
            # Prep
            count += 1
            name, ext = os.path.splitext(f)


            # Extract any tags from the filename (Tagspaces)
            tags = TAGS.search(name)
            tags = tags.group(0) if tags else ""

            # Stick it all together
            new_name = "".join((dir_name, "_", str(count).zfill(num_zeroes), tags, ext))
            new_path = os.path.join(root, new_name)
            if os.path.exists(new_path):
                continue # Crap we made a file that already exists... skip it for now.

            # Record changes
            needs_change.append((path, new_path))

    # Rename some files!
    for src, dest in needs_change:
        if not os.path.exists(dest): # One final check
            print("Renaming: \nOLD: %s\nNEW: %s" % (src, dest))
            shutil.move(src, dest)


class Main(object):

    def __init__(s):
        """ Main window! """
        # s.last_location = os.getcwd() # Where to start browsing.
        s.last_location = os.path.realpath(os.path.dirname(__file__))

        window = tkinter.Tk()
        window.title("Rename all files.")

        # Add a descriptive label
        desc_label = tkinter.Label(window, text="Rename all files in a folder, to match the folder name.")

        # Add a textbox and button
        s.path_entry = tkinter.Entry(window, width=50)
        browse_button = tkinter.Button(window, text="Browse", command=s.browse_path)

        # Add button to go!
        rename_button = tkinter.Button(window, text="Rename!", command=s.rename)

        # Put it all together!
        desc_label.pack()
        s.path_entry.pack(side=tkinter.LEFT)
        browse_button.pack(side=tkinter.LEFT)
        rename_button.pack(fill=tkinter.X)

        # Lets go!
        window.mainloop()

    def browse_path(s):
        """ Browse to find a path """
        path = filedialog.askdirectory(initialdir=s.last_location)
        if path:
            # Clear path box
            s.path_entry.delete(0, last=len(s.path_entry.get()))
            # Add new path to box
            s.path_entry.insert(0, path)

    def rename(s):
        """ Rename files within path """
        path = s.path_entry.get()
        if path:
            if os.path.isdir(path):
                # TODO: Check if root, and fail if so.
                if "yes" == messagebox.askquestion(message="Are you sure you wish to rename the files in the folder:\n\n%s" % path):
                    # Ok, we're good to go! Lets do it!
                    rename_files(path)
            else:
                messagebox.showwarning("Oh no!", "Sorry, I could not find the folder:\n\n%s" % path)
        else:
            messagebox.showinfo("Nekminnit.", "Please choose a folder to rename files within.")

if __name__ == '__main__':
    Main()
