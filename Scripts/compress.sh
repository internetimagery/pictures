#!/bin/bash
# Compress all files in folder.

# Set shell options to use multiple extensions per loop
# https://superuser.com/questions/912096/list-multiple-file-types-in-bash-for-loop
shopt -s nullglob nocaseglob


IN="src"
OUT="dst"

mkdir $IN
mkdir $OUT

# Convert images
for f in *.jpg *.jpeg *.png; do
  echo "Compressing " "$f"
  mozjpeg "$f" > "$OUT/$f"
  mv "$f" "$IN/$f"
done

# Convert movies
for f in *.mp4 *.mov; do
  o="${f%.*}.mp4"
  echo "Compressing " "$f"
  ffmpeg -i "$f" -c:v h264 -crf 23 "$OUT/$o"
  mv "$f" "$IN/$f"
done
